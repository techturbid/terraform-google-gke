resource "null_resource" "wait_for_resources" {
  triggers = {
    depends_on = join("", var.wait_for_resources)
  }
}

locals {
  cluster_location_parts = split("-", var.CLUSTER_LOCATION)
    gcp_region = "${element(local.cluster_location_parts, 0)}-${element(local.cluster_location_parts, 1)}"
    gcp_zone   = "${element(local.cluster_location_parts, 0)}-${element(local.cluster_location_parts, 1)}-${element(local.cluster_location_parts, 2)}"
}

resource "google_project_service" "cloudresourcemanager-api" {
  depends_on = ["null_resource.wait_for_resources"]

  project = var.GCP_SA_PROJECT
  service = "cloudresourcemanager.googleapis.com"
}

resource "google_project_service" "gke-api" {
  project    = var.GCP_PROJECT
  service    = "container.googleapis.com"
  depends_on = ["google_project_service.cloudresourcemanager-api"]
}

resource "google_container_cluster" "gke" {
  provider           = "google-beta"
  project            = var.GCP_PROJECT
  name               = var.CLUSTER_NAME
  location           = var.REGIONAL_CLUSTER ? local.gcp_region : local.gcp_zone
  min_master_version = var.CLUSTER_VERSION
  network            = var.vpc_network_name
  subnetwork         = var.vpc_subnetwork_name

  # Use Calico as network policy
  network_policy {
    enabled  = true
    provider = "CALICO"
  }

  #remove default since we are creating our own node pool at the below resource
  remove_default_node_pool = true
  initial_node_count       = 1

  # Disable basic authentication
  master_auth {
    username = ""
    password = ""
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  addons_config {
    # Who uses this?
    kubernetes_dashboard {
      disabled = true
    }

    http_load_balancing {
      # (true/false) Based on some examples they keep it enable by default so I did the same at vars and I will dive into this later
      disabled = var.http_load_balancing_disabled
    }

    # Enable the network policy addon for the master
    network_policy_config {
      disabled = false
    }

    istio_config {
      disabled = true
    }


  }

  # Activate IP aliases and use secondary ranges for pods and services
  ip_allocation_policy {
    use_ip_aliases                = true
    cluster_secondary_range_name  = var.cluster_secondary_range_name
    services_secondary_range_name = var.services_secondary_range_name
  }

  # I will work on this later
  //    maintenance_policy {
  //    daily_maintenance_window {
  //
  //      start_time = var.daily_maintenance_window_start_time
  //    }
  //  }

  # I will have to research a bit more on how to tag resources on GCP
  //    tags = {
  //      maintained_by = "Terraform Cloud"
  //      tf_workspace = "${var.cluster_env}-${var.cluster_name}"
  //      master_version = google_container_cluster.gke.master_version
  //      provisioner = google_container_cluster.gke.provisioner
  //      owner = var.owner
  //    }

  timeouts {
    update = "30m"
    create = "30m"
    delete = "30m"
  }

  depends_on = ["google_project_service.gke-api"]
}

resource "google_container_node_pool" "gke_nodes" {
  project  = var.GCP_PROJECT
  provider = "google-beta"
  name     = "nodes"
  location = var.REGIONAL_CLUSTER ? local.gcp_region : local.gcp_zone
  cluster  = google_container_cluster.gke.name

  #if location is a region this terraform will provision one node in each zone thus making this setting deploy 3x
  node_count = var.NODE_COUNT

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    preemptible  = var.PREEMPTIBLE_NODES
    machine_type = var.MACHINE_TYPE

    # I will have to research a bit more on how to tag resources on GCP
    //        tags = {
    //          maintained_by = "Terraform Cloud"
    //          tf_workspace = "${var.cluster_env}-${var.cluster_name}"
    //          master_name = google_container_cluster.gke.name
    //          master_version = google_container_cluster.gke.master_version
    //          #name = google_container_cluster.gke.name
    //          provisioner = google_container_node_pool.gke_nodes.provisioner
    //          node_version = google_container_node_pool.gke_nodes.version
    //          owner = var.owner
    //        }

    disk_size_gb = var.NODES_DISK_SIZE

    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }

  timeouts {
    create = "30m"
    update = "20m"
    delete = "20m"
  }
}