output "cluster_name" {
  value = google_container_cluster.gke.name
}

output "cluster_version" {
  value = google_container_cluster.gke.master_version
}

output "cluster_ca_certificate" {
  value = google_container_cluster.gke.master_auth.0.cluster_ca_certificate
}

output "cluster_endpoint" {
  value = google_container_cluster.gke.endpoint
}

output "node_pool_id" {
  value = google_container_node_pool.gke_nodes.id
}

output "google_container_cluster" {
  value = google_container_cluster.gke
}

output "google_container_node_pool" {
  value = google_container_node_pool.gke_nodes
}