Techturbid Terraform Registry - gke
---
#### GKE module used by techturbid's GKE modules like gke_base

This module will create a GKE cluster with the standard configuration for Techturbid.
It will also create a node pool with some tweakes and will use CALICO as Network Policy
 
This module requires input from the module gke_vpc 

---

The input required for this modules are:

|            **Variable**           |       **Value**       |
|-----------------------------------|:----------------------|
| CLUSTER_NAME                      |                       |
| GCP_PROJECT                      |                       |
| CLUSTER_VERSION                      |                       |
| CLUSTER_LOCATION                      |                       |
| REGIONAL_CLUSTER                      |                       |
| PREEMPTIBLE_NODES                      |                       |
| NODE_COUNT                      |                       |
| MACHINE_TYPE                      |                       |
| NODES_DISK_SIZE                      |                       |
| vpc_network_name                      |                       |
| vpc_subnetwork_name                      |                       |
| cluster_secondary_range_name                      |                       |
| services_secondary_range_name                      |                       |
| http_load_balancing_disabled                      |                       |

--- 
 
The variables with default values are:

|      **Variable**     |       **Value**     |
|-----------------------|:--------------------|
| GCP_SA_PROJECT      | techtur-bid-commons |

---

This module will output the following values:

|              **Output**            |                       **Value**               |
|------------------------------------|:----------------------------------------------|
| cluster_name                   | google_container_cluster.gke.name           |
| cluster_version                | google_container_cluster.gke.master_version |
| cluster_ca_certificate       | google_container_cluster.gke.master_auth.0.cluster_ca_certificate            |
| cluster_endpoint      | google_container_cluster.gke.endpoint            |
| node_pool_id      | google_container_node_pool.gke_nodes.id             |
| google_container_cluster                   | google_container_cluster.gke           |
| google_container_node_pool                | google_container_node_pool.gke_nodes |

---

This module requires the following providers which will be loaded by gke_base or other similar module:
```hcl-terraform
provider "google" {
  credentials = var.service-account_key
  project     = var.gcp_project
  region      = var.cluster_region
}

provider "google-beta" {
  credentials = var.service-account_key
  project = var.gcp_project
  region  = var.cluster_region

}

```

---